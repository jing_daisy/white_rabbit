<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        //TODO implement this!
       
        $text=file_get_contents($filePath);
        # change upper case to lower case
        $text=strtolower($text);
        #delete everyting that is not letters
        $text=preg_replace("/[^a-z]/","",$text);
        #count the number of each letters
        $char_dic=count_chars($text,1);

        return $char_dic;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile $key=>$value
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        //TODO implement this!
        # sort according to value
        asort($parsedFile);
        #get value and keys
        $a_values=array_values($parsedFile);
        $a_keys=array_keys($parsedFile);
        # get location of median in parsed file
        $median_index=floor(count($parsedFile)/2)-1;
        $occurrences=$a_values[$median_index];
        return chr($a_keys[$median_index]);
    
        
    }
}